# humhub-shell-tools

## Description

This repository contains some shell tools to handle easily [Humhub](https://www.humhub.com/) instances.

### humhub-update.sh

On my Humhub instance, the updater module doesn't seems to work as it doesn't detect any update for Humhub.
Also, i'm not a huge fan to do this kind of procedure manually.
To solve this problem, i created this script, based on the official update documentation : https://docs.humhub.org/docs/admin/updating/#upgrade-manually
**Please note that, at this moment backups are handled only partially so don't forget to create backups before using it !**

## Installation

This script is meant to be executed as the user that executes your PHP, usually `www-data` in default installations. I'll name it `webuser` in this documentation.

* Download the script :
```shell
wget https://framagit.org/liloumuloup/humhub-update-multitool/-/raw/main/humhub-update.sh
```
* Move the script in a path accessible by `webuser` :
```shell
mv humhub-update.sh /tmp
```
* Change owner and make it executable by `webuser` :
```shell
chown webuser:webgroup humhub-update.sh
chmod u+x humhub-update.sh
```

You're ready to go !

## Usage
### Parameters

| Parameter| Mandatory ? | Default | Description |
| ---      | ---      | ---      | ---      |
| `-w / --webroot` | No | `"/var/www"` | Webroot base directory, must be writable by `WEB_USER, absolute path |
| `-H / --humhub` | No | `"humhub"` | Humhub directory present in `WEBROOT`, relative path |
| `-u / --user` | No | `"www-data"` | User that owns `HUMHUB_ROOT` folder and executes PHP |
| `-g / --group` | No | `"www-data"` | Group that owns `HUMHUB_ROOT` folder and executes PHP |
| `-v / --version` | **Yes** | ` ` | Destination version for Humhub instance, check the [migration guide](https://docs.humhub.org/docs/admin/updating-migration2) before executing it |
| `-p / --php-path` | No | `"/usr/bin/php"` | Path to php, absolute path |
| `-h / --help` | No | ` ` | Prints parameter list |

### Default installations
```shell
sudo -u www-data bash humhub-update.sh -v 1.14.2 -p /usr/bin/php
```
### Custom webserver installation
```shell
bash humhub-update.sh -v 1.14.2 -p /opt/php8.0/bin/php -w /home/clients/xxx/sites/ -H example.com -u xxx -g xxx
```

## Support
If you need any help with this script, please feel free to open an issue and i'll answer you as soon as possible.

## Roadmap
### New tools
At this moment, there is only one tool here but i'm planning to add some others :
* Backup script
* Install script

### Improvments for update
Missing functions for `humhub-update.sh` :
* Remove backup folder whenever the update has ended
* MySQL backup
* Cron disable
* Cron enable

### Repository tools :
* Testing script in CI
* Add shellcheck CI
* Add autorelease

Also, please check the issue list to get a status from this ToDo list.

## Contributing
If you would like to contribute to this repository, feel free to do it !

## Authors and acknowledgment


## License
This project has been licenced on CC BY SA. See the LICENCE file for all details.
If you need a TL;DR, see here : [Attribution-ShareAlike 4.0 International (CC BY-SA 4.0)](https://creativecommons.org/licenses/by-sa/4.0/deed.en)

## Project status
At this moment, i created a 1st version that works to update an instance. It misses a few functions and i'll add them as soon as possible. Anyway, i'm open to pull requests.
